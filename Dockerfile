FROM python:3.10

RUN apt-get update && apt-get install -y virtualenv mercurial
RUN useradd -ms /bin/bash trosnoth
WORKDIR /opt/trosnoth

RUN hg clone https://foss.heptapod.net/games/trosnoth /opt/trosnoth --updaterev server

# For debugging, show what revision we're actually on
RUN hg log -r.


#ENV VIRTUAL_ENV=/opt/trosnoth/venv
#RUN virtualenv -p python3 $VIRTUAL_ENV
#ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN pip install -r requirements
#RUN pip install .
#RUN pip3 install --upgrade trosnoth
# RUN pip install django==2.2.* bcrypt pyasn1
RUN pip install -r server-requires
#RUN apt-get update && apt-get install -y python3-django

USER trosnoth
EXPOSE 6780 6789 8080 6722
CMD ["/opt/trosnoth/run-auth.py", "--no-browser", "-l", "/home/trosnoth/.trosnoth/logs/trosnoth.log"]
